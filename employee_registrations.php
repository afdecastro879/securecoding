<?php
include_once("_bl/_verify_employee_session.php");
include_once("_templates/head.php") ?>
	<body>
		<div id="page">
			<?php include_once("_templates/header.php") ?>
		</div>
		<div id="content">
			<div id="container">
				<div id="main">
					<?php include_once("_templates/employee/employee_menu.php") ?>
					<div id="text">
						<h1>Aprobar registros</h1>
						<p>
							<table style="margin-top: 35px">
								<tr>
									<td>Login</td>
									<td>Nombre</td>
									<td>Apellido</td>
								</tr>
								<?php

                                $registros = ClienteClass::darClientesNoAprobados();
                                    foreach ($registros as $registro){?>
									<tr>
                                        <td><?php echo $registro->login; ?></td>
                                        <td><?php echo $registro->nombre; ?></td>
                                        <td><?php echo $registro->apellido; ?></td>
                                        <td>
                                            <button onclick="aprobarRegistro('<?php echo($registro->login); ?>','<?php
                                                $data_str = "approve_account-".$_SESSION["_SecureCodingEmployeeSessionID_"];
                                                $key = hash('sha512', $_SESSION["_SecureCodingEmployeeSessionID_"]);
                                                echo (CSRFClass::request_token_generate($data_str,$key,1000000));
                                            ?>');">Aprobar</button>
                                            <button onclick="rechazarRegistro('<?php echo($registro->login); ?>','<?php
                                            $data_str = "deny_account-".$_SESSION["_SecureCodingEmployeeSessionID_"];
                                            $key = hash('sha512', $_SESSION["_SecureCodingEmployeeSessionID_"]);
                                            echo( CSRFClass::request_token_generate($data_str,$key));
                                            ?>)">Rechazar</button>
                                        </td>
									</tr>
                                    <?php
                                    }
                                    ?>
							</table>
						</p>
						<?php include_once("_templates/message_boxes.php") ?>
					</div>
				</div>
		</div>
		<?php include_once("_templates/footer.php") ?>
		</div> 
	</body>
</html>
