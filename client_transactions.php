<?php
include_once("_bl/_verify_sesion.php");
include_once("_templates/head.php");
$cliente = ClienteClass::darClientePorID($_SESSION["_SecureCodingSessionID_"]);
?>
	<body>
		<div id="page">
			<?php include_once("_templates/header.php") ?>
		</div>
		<div id="content">
			<div id="container">
				<div id="main">
					<?php include_once("_templates/client/client_menu.php") ?>
					<div id="text">
						<h1>Historial de transacciones</h1>
						<p>
							<table width = 500px>
								<tr>
									<td>Cuenta de Salida</td>
									<td>Cuenta de Llegada</td>
                                    <td>Monto</td>
                                    <td>Estado</td>
                                    <td>Fecha</td>
								</tr>
                            <?php
                            $transacciones = TransaccionClass::darTransaccionesPorCliente($cliente->login);
                            foreach($transacciones as $transaccion){?>
                                <tr>
                                    <td><?php echo $transaccion->cuentaSalida ?></td>
                                    <td><?php echo $transaccion->cuentaLlegada; ?></td>
                                    <td>$<?php echo $transaccion->monto; ?></td>
                                    <td>
                                        <?php
                                        $estado = $transaccion->aprobada;
                                        if($estado == 1){
                                            echo "Aprobada";
                                        }
                                        else if($estado==2){
                                            echo "rechazada";
                                        }
                                        else{
                                            echo "Esperando aprobación";
                                        }
                                        ?>
                                    </td>
                                    <td><?php echo $transaccion->fecha; ?></td>
                                </tr>
                            <?php
                            }
                            ?>
							</table>
						</p>
					</div>
				</div>
			</div>
			<?php include_once("_templates/footer.php") ?>
		</div> 
	</body>
</html>
