<?php
function __autoload($nombre){
    include_once "../_class/$nombre.php";
}
include_once("_verify_sesion.php");
session_start();
if(isset($_POST["cuenta_llegada"]) && isset($_POST["cuenta_salida"]) && isset($_POST["monto"]) && isset($_POST["codigo_transaccion"])
    && isset($_POST["numeral"]) && isset($_POST["token"])){

    //VERIFICACION DEL TOKEN
    $token = $_POST["token"];
    $data_str = "do_transaction-".$_SESSION["_SecureCodingSessionID_"];
    $key = hash('sha512', $_SESSION["_SecureCodingSessionID_"]);
    if(CSRFClass::request_token_verify($token,$data_str,$key)){
        $cuentaLlegada = htmlspecialchars($_POST["cuenta_llegada"]);
        $monto = htmlspecialchars($_POST["monto"]);
        $codigo = htmlspecialchars($_POST["codigo_transaccion"]);
        $cuentaSalida = htmlspecialchars($_POST["cuenta_salida"]);
        $login = $_SESSION["_SecureCodingSessionID_"];
        $numeral = htmlspecialchars($_POST["numeral"]);

        //VERIFICACION DE LOS PARAMETROS
        if(!VerificadorClass::validarAlfanumerico($login) ||
            VerificadorClass::validarVacio($login) ||
            VerificadorClass::validarLongitudMenorA($login,6) ||
            VerificadorClass::validarLongitudMayorA($login,30)||
            VerificadorClass::validarVacio($cuentaLlegada) ||
            !VerificadorClass::validarNumerico($cuentaLlegada)||
            VerificadorClass::validarVacio($monto)||
            !VerificadorClass::validarNumerico($monto)||
            !VerificadorClass::validarAlfanumerico($codigo) ||
            VerificadorClass::validarVacio($codigo) ||
            VerificadorClass::validarLongitudMenorA($codigo,15) ||
            VerificadorClass::validarVacio($cuentaSalida) ||
            !VerificadorClass::validarNumerico($cuentaSalida)||
            VerificadorClass::validarVacio($numeral)||
            !VerificadorClass::validarNumerico($numeral)
        ){
            echo "Error en los parametros";
        }
        //FIN VERIFICACION DE LOS PARAMETROS

        else{
            $cuentas = CuentaClass::darCuentasPorLogin($login);
            $cliente = ClienteClass::darClientePorID($login);

            //Verificar si la cuenta de salida corresponde a la sesion
            $termino = false;
            for($i=0;$i<count($cuentas) && !$termino;$i++) {
                $cuenta = $cuentas[$i];
                if($cuenta->numero_cuenta == $cuentaSalida){
                    $termino=true;
                }
            }
            if($termino && $cuentaLlegada != $cuentaSalida){
                $codigo_verificado = false;
                if ($cliente->tipo_transaccion == 0){
                    $codigo_verificado = CodigoClass::verificarCodigo($codigo,$numeral,$login);
                } else if ($cliente->tipo_transaccion == 1) {
                    $pin = CodigoClass::darCodigoPorNumero(1,$login)->codigo;
                    $codigo_verificado = CodigoClass::verificarCodigoGeneradoConToken($cuentaLlegada,$monto,$pin,$codigo);
                }

                if($codigo_verificado){
                    //INICIAR TRANSACCION!
                    $conexion = DataBaseClass::darConexion();
                    $conexion->autocommit(false);

                    $transaccion = new TransaccionClass();
                    $transaccion->asignarDatosTransaccion($cuentaSalida,$cuentaLlegada,$monto,0,$codigo,null);
                    $cuenta = CuentaClass::darCuentaPorNumero($cuentaSalida);
                    $cuentaDeLlegada = CuentaClass::darCuentaPorNumero($cuentaLlegada);
                    if(isset($cuentaLlegada)){
                        if($cuenta->disminuirSaldo($monto)){
                            if($monto<CodigoClass::MONTO_MAXIMO){
                                if($cuentaDeLlegada->aumentarSaldo($monto)){
                                    $transaccion->aprobada=1;
                                    if($transaccion->save()){
                                        $conexion->commit();
                                        $conexion->autocommit(true);
                                        $conexion->close();
                                        echo "success";
                                    }
                                    else{
                                        $conexion->rollback();
                                        $conexion->autocommit(true);
                                        $conexion->close();
                                        echo("Error al realizar la transacción. \n  Debes esperar 15 minutos para realizar una nueva transaccion.");
                                    }
                                }
                                else{
                                    $conexion->rollback();
                                    $conexion->autocommit(true);
                                    $conexion->close();
                                    echo("Error al realizar la transacción");
                                }
                            }
                            else{
                                echo "Tu transacción ha sido enviada y será revisada por un funcionario.";
                            }
                        }
                        else{
                            $conexion->rollback();
                            $conexion->autocommit(true);
                            $conexion->close();
                            echo "No cuentas con el saldo para realizar la transacción";
                        }
                    }
                    else{
                        $conexion->rollback();
                        $conexion->autocommit(true);
                        $conexion->close();
                        echo "La cuenta de destino no existe";
                    }
                }
                else{
                    echo "EL codigo no es válido";
                }
            }
            else{
                echo "error";
            }
        }
    }
    else{
        echo "Error de sesion";
    }
}
else{
    echo "error";
}