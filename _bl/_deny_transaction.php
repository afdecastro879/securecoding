<?php
function __autoload($nombre){
    include_once "../_class/$nombre.php";
}
include_once("_verify_employee_session.php");
if(isset($_POST["login"]) && isset($_POST["token"])){
    $codigo = htmlspecialchars($_POST["login"]);

    //VERIFICACION DEL TOKEN
    $token = $_POST["token"];
    $data_str = "deny_transaction-".$_SESSION["_SecureCodingEmployeeSessionID_"];
    $key = hash('sha512', $_SESSION["_SecureCodingEmployeeSessionID_"]);
    if(CSRFClass::request_token_verify($token,$data_str,$key)){
        //VERIFICACION DE LOS PARAMETROS
        if(!VerificadorClass::validarAlfanumerico($codigo) ||
            VerificadorClass::validarVacio($codigo) ||
            VerificadorClass::validarLongitudMenorA($login,128) ||
            VerificadorClass::validarLongitudMayorA($login,128)){
            echo "Error en los parametros";
        }
        //FIN VERIFICACION DE LOS PARAMETROS

        else{
            //INICIA LA TRANSACCION
            $conexion = DataBaseClass::darConexion();
            $conexion->autocommit(false);
            if(TransaccionClass::rechazarTransaccion($codigo)){
                $transaccion = TransaccionClass::darTransaccionesPorCodigo($codigo);
                $cuentaSalida = $transaccion->cuentaSalida;
                $cuenta = CuentaClass::darCuentaPorNumero($cuentaSalida);
                if(isset($cuenta)){
                    if($cuenta->aumentarSaldo($transaccion->monto)){
                        $conexion->commit();
                        $conexion->autocommit(true);
                        $conexion->close();
                        echo "success";
                    }
                    else{
                        $conexion->rollback();
                        $conexion->autocommit(true);
                        $conexion->close();
                        echo "Error al intentar rechazar. Por Favor intente mas tarde";
                    }
                }
                else{
                    $conexion->rollback();
                    $conexion->autocommit(true);
                    $conexion->close();
                    echo "Error al intentar rechazar. Por Favor intente mas tarde";
                }
            }
            else{
                $conexion->rollback();
                $conexion->autocommit(true);
                $conexion->close();
                echo "Error al intentar rechazar. Por Favor intente mas tarde";
            }
        }
    }
    else{
        echo "Error de sesión";
    }
}
else{
    echo "error";
}