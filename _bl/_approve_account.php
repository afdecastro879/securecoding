<?php
function __autoload($nombre){
    include_once "../_class/$nombre.php";
}
include_once("_verify_employee_session.php");
include_once("../_pdfGen/src/Cezpdf.php");
if(isset($_POST["login"]) && isset($_POST["token"])){
    $login = htmlspecialchars($_POST["login"]);

    //VERIFICACION DEL TOKEN
    $token = $_POST["token"];
    $data_str = "approve_account-".$_SESSION["_SecureCodingEmployeeSessionID_"];
    $key = hash('sha512', $_SESSION["_SecureCodingEmployeeSessionID_"]);
    if(CSRFClass::request_token_verify($token,$data_str,$key)){
        //VERIFICACION DE LOS PARAMETROS
        if(!VerificadorClass::validarAlfanumerico($login) ||
            VerificadorClass::validarVacio($login) ||
            VerificadorClass::validarLongitudMenorA($login,6) ||
            VerificadorClass::validarLongitudMayorA($login,30)){
            echo "Error en los parametros";
        }
        //FIN VERIFICACION DE LOS PARAMETROS
        else{
            $cliente = ClienteClass::darClientePorID($login);
            if(isset($cliente) && $cliente->aprobado == 0){
                $conexion = DataBaseClass::darConexion();
                $conexion->autocommit(false);
                //INICIA LA TRANSACCION
                if($cliente->aprobarRegistro()){
                    $cuenta = new CuentaClass();
                    $cuenta->asignarDatos($login,10000);
                    if($cuenta->save()){
                        require '../_mailer/PHPMailerAutoload.php';

                        $mail = new PHPMailer;

                        $mail->isSMTP();                                      // Set mailer to use SMTP
                        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
                        $mail->SMTPAuth = true;                               // Enable SMTP authentication
                        $mail->Username = 'securecoders123@gmail.com';                 // SMTP username
                        $mail->Password = 'securecoders';                           // SMTP password
                        $mail->SMTPSecure = 'tls';              // Enable encryption, 'ssl' also accepted

                        $mail->From = 'securecoders123@gmail.com';
                        $mail->FromName = 'Warrior Bank';
                        $mail->addAddress($cliente->correo, $cliente->nombre." ".$cliente->apellido);     // Add a recipient

                        $mail->isHTML(true);                                  // Set email format to HTML

                        $mail->Subject = 'Bienvenido';
                        $error = false;
                        include_once ('../_class/CodigoClass.php');
                        if($cliente->tipo_transaccion == 0){
                            $codigos = CodigoClass::generarCodigos($login);
                            if(!empty($codigos)){
                                $conexion->commit();
                                $conexion->autocommit(true);
                                $conexion->close();
                                $useEncryption = true;
                                $doc = generatePDF($codigos, $useEncryption, $login);
                                $pdfFileName = "../_tmp/tmpPDf.pdf";
                                $fh = fopen($pdfFileName, 'w');
                                fwrite($fh, $doc);
                                fclose($fh);

                                $message = "¡Hola ".$cliente->nombre."! ¡Saludos  desde Warrior Bank!</br>";
                                $message .= "<p>¡Felicidades, su cuenta ha sido aprobada!</p>";
                                if ($useEncryption) {
                                    $message .= "<p>A continuación encontrará un pdf encriptado con la lista de códigos. Esto códigos son importantes, consérvelos en un lugar seguro.</p>";
                                    $message .= "<p>Utlice su LOGIN que usó para registrarse para abrir el archivo pdf.</p>";
                                } else  {
                                    $message .= "<p>A continuación encontrará un pdf con la lista de códigos. Esto códigos son importantes, consérvelos en un lugar seguro.</p>";
                                }

                                $mail->Body    = $message;
                                $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

                                $mail->addAttachment($pdfFileName, "ListaCodigos.pdf");
                            }
                            else{
                                $conexion->rollback();
                                $conexion->autocommit(true);
                                $conexion->close();
                                $error=true;
                                echo "Error al generar codigos";
                            }
                        }else  if($cliente->tipo_transaccion == 1){
                            $conexion->commit();
                            $conexion->autocommit(true);
                            $conexion->close();

                            $pin = md5(uniqid(mt_rand(), true));
                            CodigoClass::insertarCodigoToken($pin,1,$login);
                            $numero = $cuenta->numero_cuenta;
                            $i=0;
                            $array=array();
                            $resultado = exec("./generateJar.sh  $numero $pin",$array,$i);
                            $j=0;
                            $array2=array();
                            $resultado2 = exec("./obfuscate.sh $numero",$array2,$j);
                            $jar_file_name = "../_tmp/$numero.jar";
                            $jar_file_name_out = "../_tmp/$numero"."_out.jar";

                            $message = "¡Hola ".$cliente->nombre."! ¡Saludos  desde Warrior Bank!</br>";
                            $message .= "<p>¡Felicidades, su cuenta ha sido aprobada!</p>";
                            $message .= "<p>Adjunto a este correo podrá encontrar un programa que le permitirá realizar sus transacciones.</p>";
                            $message .= "<p>Genere su código con este programa e introduzcalo a través de nuestra plataforma web.</p>";

                            $mail->Body    = $message;
                            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

                            //TODO AGREGAR PROGRAMA JAVA!!!
                            $mail->addAttachment($jar_file_name_out, "token.jar");
                        }

                        if($error || !$mail->send()) {
                            echo 'Message could not be sent.';
                            echo 'Mailer Error: ' . $mail->ErrorInfo;
                        } else {
                            if($cliente->tipo_transaccion == 0){
                                unlink($pdfFileName);
                            } else if($cliente->tipo_transaccion == 1){
                                unlink($jar_file_name);
                                unlink($jar_file_name_out);
                                unlink ("../_bl/myconfig2");
                            }
                            echo("success");
                        }
                    }
                    else{
                        $conexion->rollback();
                        $conexion->autocommit(true);
                        $conexion->close();
                        echo("Error al aprobar la cuenta");
                    }
                }
                else{
                    $conexion->rollback();
                    $conexion->autocommit(true);
                    $conexion->close();
                    echo "error";
                }
            }
            else{
                echo "error";
            }
        }
    }
    else{
        echo "Error de sesión";
    }
}
else{
    echo "error";
}

function generatePDF($codigos, $allowEncryption, $password)
{
	$pdf = new Cezpdf('a4', 'portrait');
	$pdf->tempPath  = '../_tmp/';
	$pdf->selectFont('FreeSerif','', 1, true);
	$pdf->ezSetCmMargins(2,2.5,2,2);
	
	if ($allowEncryption) {
		$pdf->setEncryption($password, "", array('copy','print'));
	}
	
	$pdf->ezText("Warrior Bank", 50, array("justification"=>"center"));
	$pdf->ezText("¡Gracias por confiar en nosotros!",
		16,
		array("justification"=>"center", "spacing"=>1.5));
	$pdf->ezText("Aquí se encuentra la lista de los códigos que necesita para aprobar transacciones.",
		14,
		array("spacing"=>1.5, "justification"=>"full"));
	$pdf->ezText("Es importante conservar éstos códigos. Consérvelos en un lugar seguro. Cada vez que se usa un código ya no podrá volver a usarse.",
		14,
		array("spacing"=>1, "justification"=>"full"));
	
	$cols = array('num'=>'No', 'codigo'=>'Código');
	$data = array();
	
	$i = 0;
	foreach($codigos as $codigo){
		$data[$i] = array('num'=>($i+1),'codigo'=>$codigo);
		$i++;
	}	
	
	$pdf->ezTable($data, $cols, 'Lista de códigos', array('width' =>470));
	return $pdf->ezOutput();
}