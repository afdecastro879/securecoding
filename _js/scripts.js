
/* From scripts */   
function hide_normal_form() 
{
	var form = document.getElementById('normal_form');
	form.style.display = 'none';
	
	var form2 = document.getElementById('archive_form');
	form2.style.display = 'block';
}
  
function hide_archive_form() 
{
	var form = document.getElementById('normal_form');
	form.style.display = 'block';
	
	var form2 = document.getElementById('archive_form');
	form2.style.display = 'none';    
}

/**
 * Valida Si el campo con identificador id está vacío.
 * @param id El identificador del campo a evaluar
 * @param mensaje Un indicador del nombre del campo
 * @param errorId El id del campo de error donde se mostrará el mensaje resultado de la validación
 * El elemento errorId debe tener un hijo con id <errorId>Text
 * @returns {boolean} true si es campo está vacío false de lo contrario
 */
function validarVacio(id, mensaje, errorId){
    var input = $("#"+id);
    if(input.val().trim() == ""){
        $("#"+errorId+"Text").text("El campo " +mensaje +" no puede estar vacío");
        mostrarElementoVisibilityOFade(errorId);
        input.focus();
        focusOnError(id);
        return false;
    }
    return true;
}

/**
 * Valida si el campo con identificador id está vacío
 * @param id El identificador del campo a evaluar
 * @returns {boolean} true si el campo está vacío false de lo contrario.
 */
function validarVacioDefault(id){
    var input = $("#"+id);
    if(input.val().trim() == ""){
        return false;
    }
    return true;
}

/**
 * Valida si el checkBox con identificador id esta seleccionado o no.
 * @param id El identificador del checkbox a evaluar
 * @param mensaje El mensaje que se mostrará en el campo de error con identificador errorId
 * @param errorId el id del campo de error donde se mostrará el mensaje resultado de la validación
 * El elemento errorId debe tener un hijo con id <errorId>Text
 * @returns {boolean} true si el campo esta checkeado, false de lo contrario
 */
function validarCheckBox(id, mensaje, errorId){
    var input = $("#"+id);
    if(!input.is(":checked")){
        $("#"+errorId+"Text").text(mensaje);
        mostrarElementoVisibilityOFade(errorId);
        input.focus();
        return false;
    }
    return true;
}

/**
 * Valida si el checkBox con identificador id esta seleccionado o no.
 * @param id El identificador del checkbox a evaluar
 * @returns {boolean} true si el campo esta checkeado, false de lo contrario
 */
function validarCheckBoxDefault(id)
{
    var input = $("#"+id);
    if(!input.is(":checked")){
        return false;
    }
    return true;
}

/**
 * Valida si el contenido del campo con identificador id contiene solo caracteres numéricos
 * @param id El identificador del del campo a evaluar.
 * @param mensaje Un indicador del nombre del campo
 * @param errorId el id del campo de error donde se mostrará el mensaje resultado de la validación
 * El elemento errorId debe tener un hijo con id <errorId>Text
 * @returns {boolean} true si el campo solo contiene caracteres numéricos, false de lo contrario
 */
function validarNumerico(id, mensaje, errorId){
    var re = /^[0-9]+$/;
    var input = $("#"+id);
    var valor = input.val();
    if(!re.test(valor))
    {
        $("#"+errorId+"Text").text("El campo "+mensaje+" debe contener solo caracteres numéricos");
        mostrarElementoVisibilityOFade(errorId);
        input.focus();
        focusOnError(id);
        return false;
    }
    return true;
}

/**
 * Valida si el contenido del campo con identificador id contiene solo caracteres numéricos
 * @param id El identificador del del campo a evaluar.
 * @returns {boolean} true si el campo solo contiene caracteres numéricos, false de lo contrario
 */
function validarNumericoDefault(id){
    var re = /^[0-9]+$/;
    var input = $("#"+id);
    var valor = input.val();
    if(!re.test(valor))
    {
        return false;
    }
    return true;
}

/**
 * Valida si el contenido del campo con identificador id contiene un correo válido
 * @param id El identificador del del campo a evaluar.
 * @param mensaje Un indicador del nombre del campo
 * @param errorId el id del campo de error donde se mostrará el mensaje resultado de la validación
 * El elemento errorId debe tener un hijo con id <errorId>Text
 * @returns {boolean} true si el campo contiene un correo válido, false de lo contrario
 */
function validarCorreoUniandes(id, mensaje, errorId)
{
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))$/;
    var input = $("#"+id);
    var valor = input.val();
    if(!re.test(valor))
    {
        $("#"+errorId+"Text").text("El campo "+mensaje+" no contiene un correo válido");
        mostrarElementoVisibilityOFade(errorId);
        input.focus();
        focusOnError(id);
        return false;
    }
    return true;
}

/**
 * Valida si el contenido del campo con identificador id contiene un correo válido
 * @param id El identificador del del campo a evaluar.
 * @returns {boolean} true si el campo contiene un correo válido, false de lo contrario
 */
function validarCorreoDefault(id)
{
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var input = $("#"+id);
    var valor = input.val();
    if(!re.test(valor))
    {
        return false;
    }
    return true;
}

function validarCorreoDefaultConError(id, mensaje, errorId)
{
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var input = $("#"+id);
    var valor = input.val();
    if(!re.test(valor))
    {
        $("#"+errorId+"Text").text("El campo "+mensaje+" no contiene un correo válido");
        mostrarElementoVisibilityOFade(errorId);
        input.focus();
        focusOnError(id);
        return false;
    }
    return true;
}


/**
 * Valida si el contenido del campo con identificador id contiene solo caracteres alfabeticos
 * @param id El identificador del del campo a evaluar.
 * @param mensaje Un indicador del nombre del campo
 * @param errorId el id del campo de error donde se mostrará el mensaje resultado de la validación
 * El elemento errorId debe tener un hijo con id <errorId>Text
 * @returns {boolean} true si el campo contiene solo caracteres alfabeticos, false de lo contrario
 */
function validarAlfabetico(id, mensaje, errorId){
    var re = /^[ñA-Za-z 'áéíóú]+$/;
    var input = $("#"+id);
    var valor = input.val();
    if(!re.test(valor))
    {
        $("#"+errorId+"Text").text("El campo "+mensaje+" debe contener solo letras");
        mostrarElementoVisibilityOFade(errorId);
        input.focus();
        focusOnError(id);
        return false;
    }
    return true;
}

/**
 * Valida si el contenido del campo con identificador id contiene solo caracteres alfabeticos
 * @param id El identificador del del campo a evaluar.
 * @returns {boolean} true si el campo contiene solo caracteres alfabeticos, false de lo contrario
 */
function validarAlfabeticoDefault(id){
    var re = /^[ñA-Za-z 'áéíóú]+$/;
    var input = $("#"+id);
    var valor = input.val();
    if(!re.test(valor))
    {
        return false;
    }
    return true;
}

/**
 * Valida si el contenido del campo con identificador id contiene solo caracteres alfanuméricos
 * @param id El identificador del campo a evaluar.
 * @param mensaje Un indicador del nombre del campo
 * @param errorId el id del campo de error donde se mostrará el mensaje resultado de la validación
 * El elemento errorId debe tener un hijo con id <errorId>Text
 * @returns {boolean} true si el campo contiene solo caracteres alfanuméricos, false de lo contrario
 */
function validarAlfaNumerico(id, mensaje, errorId){
    var re = /^[ñ0-9A-Za-z 'áéíóú]+$/;
    var input = $("#"+id);
    var valor = input.val();
    if(!re.test(valor))
    {
        $("#"+errorId+"Text").text("El campo "+mensaje+" debe contener solo letras");
        mostrarElementoVisibilityOFade(errorId);
        input.focus();
        focusOnError(id);
        return false;
    }
    return true;
}

/**
 * Valida si el contenido del campo con identificador id contiene solo caracteres alfanuméricos
 * @param id El identificador del campo a evaluar.
 * @returns {boolean} true si el campo contiene solo caracteres alfanuméricos, false de lo contrario
 */
function validarAlfaNumericoDefault(id){
    var re = /^[ñ0-9A-Za-z 'áéíóú]+$/;
    var input = $("#"+id);
    var valor = input.val();
    if(!re.test(valor))
    {
        return false;
    }
    return true;
}

/**
 * Valida si el contenido del campo con identificador id tiene al menos longitud caracteres.
 * @param longitud la longitud a evaluar
 * @param id El identificador del campo a evaluar
 * @param mensaje Un indicador del nombre del campo
 * @param errorId El id del campo de error donde se mostrará el mensaje de la validación
 * El elemento errorId debe tener un hijo con id <errorId>Text
 * @returns {boolean} true si el campo contiene al menos longitud caracteres, false de lo contrario.
 */
function validarLongitud(longitud, id, mensaje, errorId)
{
    var input = $("#"+id);
    var valor = input.val();
    if(valor.length<longitud)
    {
        $("#"+errorId+"Text").text("El campo "+mensaje+" debe contener al menos "+ longitud+ " caracteres");
        mostrarElementoVisibilityOFade(errorId);
        input.focus();
        focusOnError(id);
        return false;
    }
    return true;
}

/**
 * Valida si el contenido del campo con identificador id tiene al menos longitud caracteres.
 * @param longitud la longitud a evaluar
 * @param id El identificador del campo a evaluar
 * @returns {boolean} true si el campo contiene al menos longitud caracteres, false de lo contrario.
 */
function validarLongitudDefault(longitud, id)
{
    var input = $("#"+id);
    var valor = input.val();
    if(valor.length<longitud)
    {
        return false;
    }
    return true;
}

/**
 * Valida si el contenido del campo con identificador id tiene al menos longitud caracteres.
 * @param longitud la longitud a evaluar
 * @param id El identificador del campo a evaluar
 * @param mensaje Un indicador del nombre del campo
 * @param errorId El id del campo de error donde se mostrará el mensaje de la validación
 * El elemento errorId debe tener un hijo con id <errorId>Text
 * @returns {boolean} true si el campo contiene al menos longitud caracteres, false de lo contrario.
 */
function validarLongitudMenorA(longitud, id, mensaje, errorId)
{
    var input = $("#"+id);
    var valor = input.val();
    if(valor.length>longitud)
    {
        $("#"+errorId+"Text").text("El campo "+mensaje+" debe contener máximo "+ longitud+ " caracteres");
        mostrarElementoVisibilityOFade(errorId);
        input.focus();
        focusOnError(id);
        return false;
    }
    return true;
}

/**
 * Valida si los strings contenidas en el campo id1 e id2 son iguales
 * @param id1 El identificador del primer campo a evaluar
 * @param id2 El identificador del segundo campo a evaluar
 * @param errorId El id del campo de error donde se mostrará el mensaje de la validación
 * El elemento errorId debe tener un hijo con id <errorId>Text
 * @returns {boolean} true si ambos campos son iguales, false de lo contrario
 */
function validarSiStringsSonIguales(id1, id2, errorId, mensaje){
    var pass1 = $("#"+id1);
    var pass2 = $("#"+id2);
    if(pass1.val() != pass2.val()){
        $("#"+errorId+"Text").text(mensaje);
        mostrarElementoVisibilityOFade(errorId);
        pass1.val("");
        pass2.val("");
        pass1.focus();
        focusOnError(id1);
        focusOnError(id2);
        return false;
    }
    return true;
}

/**
 * Valida si los strings contenidas en el campo id1 e id2 son iguales
 * @param id1 El identificador del primer campo a evaluar
 * @param id2 El identificador del segundo campo a evaluar
 * @returns {boolean} true si ambos campos son iguales, false de lo contrario
 */
function validarSiStringsSonIgualesDefault(id1, id2){
    var pass1 = $("#"+id1);
    var pass2 = $("#"+id2);
    if(pass1.val() != pass2.val()){
        return false;
    }
    return true;
}

/**
 * Oculta el campo con identificador id
 * @param id el campo a ocultar.
 */
function ocultarVisibilityOFade(id){
    if(document.getElementById(id).style.visibility=='visible'){
        document.getElementById(id).style.visibility='hidden';
    }
    else{
        $("#"+id).fadeOut();
    }
}

/**
 * Muestra el campo con identificador id
 * @param id id del campo a mostrar.
 */
function mostrarElementoVisibilityOFade(id){
    if(document.getElementById(id).style.visibility=='hidden'){
        document.getElementById(id).style.visibility='visible';
    }
    else{
        $("#"+id).fadeIn();
    }
}

/**
 * Regirige a la página pagina
 * @param pagina la página a redirigir.
 */
function redirigir(pagina){
    location.href = pagina;
}

function limpiarCajasDeMensaje(){
    ocultarVisibilityOFade("error");
    ocultarVisibilityOFade("success");
}

function mostrarFlotanteError(mensaje){
    $("#errorText").text(mensaje);
    mostrarElementoVisibilityOFade("error")
}

function mostrarFlotanteSuccess(mensaje){
    $("#successText").text(mensaje);
    mostrarElementoVisibilityOFade("success")
}

function focusOnError(id){
    $("#"+id).addClass("inputErrorFocus");
    $("#label_"+id).addClass("spanErrorFocus");
    $("#error").attr("tabindex",-1).focus();
}


function quitarFocusOnError(idInput, idSpan){
    $("#"+idInput).removeClass("inputErrorFocus");
    $("#"+idSpan).removeClass("spanErrorFocus");
}

function validaRegistro(){
    limpiarCajasDeMensaje();
    return validarVacio("login","Login","error") &&
        validarLongitud(6,"login","Login","error")&&
        validarAlfaNumerico("login","Login","error")&&
        validarVacio("pass","Password","error") &&
        validarLongitud(6,"pass","Password","error")&&
        validarAlfaNumerico("pass","Password","error") &&
        validarVacio("repass","Password","error") &&
        validarLongitud(6,"repass","Password","error")&&
        validarAlfaNumerico("repass","Password","error") &&
        validarSiStringsSonIguales("pass","repass","error","Las contraseñas no coinciden") &&
        validarVacio("nombre","Nombre","error") &&
        validarLongitud(2,"nombre","Nombre","error")&&
        validarAlfabetico("nombre","Nombre","error")&&
        validarVacio("apellido","Apellido","error") &&
        validarLongitud(2,"apellido","Apellido","error")&&
        validarAlfabetico("apellido","Apellido","error")&&
        validarVacio("correo","Correo","error")&&
        validarCorreoDefaultConError("correo","Correo","error")&&
        validarVacio("recorreo","Contraseña","error")&&
        validarCorreoDefaultConError("recorreo","Correo","error")&&
        validarSiStringsSonIguales("correo","recorreo","error","Los correos no coinciden")&&
        validarVacio("telefono","Telefono","error") &&
        validarLongitud(7,"telefono","Telefono","error")&&
        validarLongitudMenorA(10,"telefono","Telefono","error")&&
        validarNumerico("telefono","Telefono","error")&&
        validarVacio("direccion","Dirección","error")
}

function validarLogin(){
    limpiarCajasDeMensaje();
    return validarVacio("login","Login","error")&&
        validarVacio("pass","Password","error");
}

function validarTransaccion(){
    limpiarCajasDeMensaje();
    return validarVacio("cuenta_destino","Cuenta Destino","error")&&
        validarNumerico("cuenta_destino","Cuenta Destino","error")&&
        validarVacio("monto","Monto","error")&&
        validarNumerico("monto","Monto","error")&&
        validarVacio("codigo","Codigo","error");
}

function verificarDatosTransaccion(){
    if(validarTransaccion()){
        var salida = $('#cuenta_salida').val();
        var destino = $('#cuenta_destino').val();
        var monto = $('#monto').val();
        var codigo = $('#codigo').val();
        var numeral = $('#numeral').val();
        var token = $('#token').val();
		
		$("#waitText").text("Espere un momento");
		$("#wait").fadeIn();
		$('#text :input').attr('disabled', true);
		
        $.post("_bl/_do_transaction.php",{cuenta_salida:salida, cuenta_llegada:destino,
            monto: monto, codigo_transaccion: codigo, numeral: numeral,token:token},function(res){
            if(res=="success"){
                $("#successText").text("Transacción enviada correctamente");
                $("#success").fadeIn();
                setTimeout(function(){
                    location.href = '/client.php';
                },2000);
            }else if(res == "Tu transacción ha sido enviada y será revisada por un funcionario."){
                $("#successText").text(res);
                $("#success").fadeIn();
                setTimeout(function(){
                    location.href = '/client.php';
                },2000);
            }
            else{
				$('#text :input').attr('disabled', false);
                $("#errorText").text(res);
                $("#error").fadeIn();
            }
        })
    }
}

function verificarDatosLogin(){
    if(validarLogin()){
        var login = $('#login').val();
        var pass = $('#pass').val();
		$('#text :input').attr('disabled', true);
        $.post("_bl/_do_client_login.php",{login:login,pass:pass},function(res){
            if(res=="success"){
                $("#successText").text("Bienvenido");
                $("#success").fadeIn();
                setTimeout(function(){
                    location.href = '/client.php';
                },2000);
            }
            else{
				$('#text :input').attr('disabled', false);
                $("#errorText").text(res);
                $("#error").fadeIn();
            }
        })
    }
}

function verificarDatosLoginEmployee(){
    if(validarLogin()){
        var login = $('#login').val();
        var pass = $('#pass').val();
		$('#text :input').attr('disabled', true);
        $.post("_bl/_do_employee_login.php",{login:login,pass:pass},function(res){
            if(res=="success"){
                $("#successText").text("Bienvenido");
                $("#success").fadeIn();
                setTimeout(function(){
                    location.href = '/employee.php';
                },2000);
            }
            else{
				$('#text :input').attr('disabled', false);
                $("#errorText").text(res);
                $("#error").fadeIn();
            }
        })
    }
}

function verificarDatosRegistro(){
    if(validaRegistro()){
        var nombres = $("#nombre").val();
        var apellidos = $("#apellido").val();
        var contrasenia = $("#pass").val();
        var recontrasenia = $("#repass").val();
        var email = $("#correo").val();
        var reemail = $("#recorreo").val();
        var login = $("#login").val();
        var telefono = $("#telefono").val();
        var direccion = $("#direccion").val();
        var tipo = $("#tipo").val();
		$('#text :input').attr('disabled', true);
        $.post("_bl/_do_register.php",{nombre:nombres, apellido:apellidos,
            pass:contrasenia, repass:recontrasenia, correo: email, recorreo: reemail, login: login, telefono: telefono, direccion: direccion,tipo:tipo}, function(res){
            if(res=="success"){
                $("#successText").text("Te has registrado con éxito");
                $("#success").fadeIn();
                setTimeout(function(){
                    location.href = '/register_success.php';
                },1000);
            }
            else{
				$('#text :input').attr('disabled', false);
                $("#errorText").text(res);
                $("#error").fadeIn();
            }
        });
    }
}

function verificarDatosRegistroFuncionario(){
    if(validaRegistroFuncionario()){
        var nombres = $("#nombre").val();
        var apellidos = $("#apellido").val();
        var contrasenia = $("#pass").val();
        var recontrasenia = $("#repass").val();
        var login = $("#login").val();
		$('#text :input').attr('disabled', true);
	
        $.post("_bl/_do_register_employee.php",{nombre:nombres, apellido:apellidos,
            pass:contrasenia, repass:recontrasenia, login: login}, function(res){
            if(res=="success"){
                $("#successText").text("Te has registrado con éxito");
                $("#success").fadeIn();
                setTimeout(function(){
                    location.href = './register_success_admin.php';
                },1000);
            }
            else{
				$('#text :input').attr('disabled', false);
                $("#errorText").text(res);
                $("#error").fadeIn();
            }
        });
    }
}

function validaRegistroFuncionario(){
    limpiarCajasDeMensaje();
    return validarVacio("login","Login","error") &&
        validarLongitud(6,"login","Login","error")&&
        validarAlfaNumerico("login","Login","error")&&
        validarVacio("pass","Password","error") &&
        validarLongitud(6,"pass","Password","error")&&
        validarAlfaNumerico("pass","Password","error") &&
        validarVacio("repass","Password","error") &&
        validarLongitud(6,"repass","Password","error")&&
        validarAlfaNumerico("repass","Password","error") &&
        validarSiStringsSonIguales("pass","repass","error","Las contraseñas no coinciden") &&
        validarVacio("nombre","Nombre","error") &&
        validarLongitud(2,"nombre","Nombre","error")&&
        validarAlfaNumerico("nombre","Nombre","error")&&
        validarVacio("apellido","Apellido","error") &&
        validarLongitud(2,"apellido","Apellido","error")&&
        validarAlfaNumerico("apellido","Apellido","error");
}

function aprobarRegistro(login, token){
	$("#waitText").text("Espere un momento");
    $("#wait").fadeIn();
	$('#text :input').attr('disabled', true);
	
    $.post("_bl/_approve_account.php",{login:login, token:token}, function(res){
		$("#wait").fadeOut();
        if(res=="success"){
            $("#successText").text("Cuenta aprobada");
            $("#success").fadeIn();
            setTimeout(function(){
               location.href = '/employee_registrations.php'
            },3000);
        }
        else{
			$('#text :input').attr('disabled', false);
            $("#error").fadeIn();
			$("#errorText").text(res);
            setTimeout(function(){
                limpiarCajasDeMensaje();
            },3000);
        }
    });
}

function rechazarRegistro(login,token){
	$("#waitText").text("Espere un momento");
    $("#wait").fadeIn();
	$('#text :input').attr('disabled', true);
	
    $.post("_bl/_deny_account.php",{login:login,token:token}, function(res){
		$("#wait").fadeOut();
        if(res=="success"){
            $("#successText").text("Cuenta rechazada");
            $("#success").fadeIn();
            setTimeout(function(){
                location.href = '/employee_registrations.php'
            },3000);
        }
        else{
			$('#text :input').attr('disabled', false);
			$("#success").fadeIn();
            $("#errorText").text(res);
            $("#error").fadeIn();
            setTimeout(function(){
                limpiarCajasDeMensaje();
            },3000);
        }
    });
}

function aprobarTransaccion(login,token){
	$("#waitText").text("Espere un momento");
    $("#wait").fadeIn();
	$('#text :input').attr('disabled', true);
	
    $.post("_bl/_approve_transaction.php",{codigo:login,token:token}, function(res){
		$("#wait").fadeOut();
        if(res=="success"){
            $("#successText").text("Transaccion aprobada");
            $("#success").fadeIn();
            setTimeout(function(){
                location.href = '/employee_transactions.php'
            },3000);
        }
        else{
			$('#text :input').attr('disabled', false);
			$("#success").fadeIn();
            $("#errorText").text(res);
            $("#error").fadeIn();
            setTimeout(function(){
                limpiarCajasDeMensaje();
            },3000);
        }
    });
}

function rechazarTransaccion(login,token){
	$("#waitText").text("Espere un momento");
    $("#wait").fadeIn();
	$('#text :input').attr('disabled', true);
	
    $.post("_bl/_deny_transaction.php",{login:login,token:token}, function(res){
		$("#wait").fadeOut();
        if(res=="success"){
            $("#successText").text("Transaccion rechazada");
            $("#success").fadeIn();
            setTimeout(function(){
                location.href = '/employee_transactions.php'
            },3000);
        }
        else{
			$('#text :input').attr('disabled', false);
			$("#success").fadeIn();
            $("#errorText").text(res);
            $("#error").fadeIn();
            setTimeout(function(){
                limpiarCajasDeMensaje();
            },3000);
        }
    });
}