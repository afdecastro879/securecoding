<?php
/**
 * Created by PhpStorm.
 * User: samurai
 * Date: 6/27/14
 * Time: 7:43 AM
 */

class VerificadorClass {

    public static function validarVacio($string){
        return empty($string);
    }

    public static function validarNumerico($num){
        return preg_match("/^[[:digit:]]+$/",$num);
    }

    public static function validarCorreo($string){
        $regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';
        return preg_match($regex,$string);
    }

    public static function validarAlfabetico($string){
        return preg_match("/^[a-z]+$/i",$string);
    }

    public static function validarAlfanumerico($string){
        return !preg_match('/[^a-z_\-0-9]/i',$string);
    }

    public static function validarLongitudMenorA($string, $long){
        return (strlen($string)<$long);
    }

    public static function validarLongitudMayorA($string, $long){
        return (strlen($string)>$long);
    }

    public static function validarSiStringsSonIguales($string1,$string2){
        return ($string1==$string2);
    }
} 