<?php
/**
 * Created by PhpStorm.
 * User: Andres
 * Date: 17/06/14
 * Time: 10:00 AM
 */

include_once 'DataBaseClass.php';

class ClienteClass{
    //--------------------------------------------------------
    // Atributos
    //--------------------------------------------------------

    /**
     * El login del cliente
     */
    private $login;

    /**
     * El pass del cliente
     */
    private $pass;

    /**
     * Nombre del cliente
     */
    private $nombre;

    /**
     * Apellido del cliente
     */
    private $apellido;

    /**
     * Telefono del cliente
     */
    private $telefono;

    /**
     * Direccion del cliente
     */
    private $direccion;

    /**
     * Correo del cliente
     */
    private $correo;

    /**
     * Registro aprobado del cliente (Indica si el cliente ha sido aprobado o no)
     */
    private $aprobado;

    private $tipo_transaccion;

    //--------------------------------------------------------
    // Constructor
    //--------------------------------------------------------

    /**
     * Constructor de la clase
     */
    function __construct()
    {
    }

    //--------------------------------------------------------
    // Getter and Setters
    //--------------------------------------------------------

    /**
     * Getter de los atributos
     * @param $property - el nombre del atributo
     * @return mixed - el valor del atributo
     */
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    /**
     * Setter de los atributos
     * @param $property - el nombre del atributo
     * @param $value - el nuevo valor
     */
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

    //--------------------------------------------------------
    // Métodos Estaticos
    //--------------------------------------------------------

    /**
     * @param $preparedSentence mysqli_stmt Una sentencia preparada
     * @return array
     */
    private static function getAList($preparedSentence){
        $clientes = array();
        $login = $password = $nombre = $apellido = $telefono = $direccion = $correo = $aprobada = $tipo_transaccion = null;
        if($preparedSentence->execute()){
            $preparedSentence->bind_result($login,$password,$nombre,$apellido,$telefono,$direccion,$correo,$aprobada,$tipo_transaccion);
            while($preparedSentence->fetch())
            {
                $cliente = new ClienteClass();
                $cliente->asignarDatosCliente($login,$password,$nombre,$apellido,$telefono,$direccion,$correo,$aprobada,$tipo_transaccion);
                array_push($clientes,$cliente);
            }
        }
        $preparedSentence->close();
        return $clientes;
    }

    /**
     * @param $preparedSentence mysqli_stmt Una sentencia preparada
     * @return ClienteClass|null
     */
    private static function getAnElement($preparedSentence){
        $cliente = null;
        $login = $password = $nombre = $apellido = $telefono = $direccion = $correo = $aprobada = $tipo_transaccion = null;
		if($preparedSentence->execute()){
            $preparedSentence->bind_result($login,$password,$nombre,$apellido,$telefono,$direccion,$correo,$aprobada,$tipo_transaccion);
            if($arr = $preparedSentence->fetch())
            {
                $cliente = new ClienteClass();
                $cliente->asignarDatosCliente($login,$password,$nombre,$apellido,$telefono,$direccion,$correo,$aprobada,$tipo_transaccion);
            }
        }
        return $cliente;
    }

    public static function lista(){
        $conexion = DataBaseClass::darConexion();
        $stmt = $conexion->prepare("SELECT LOGIN, PASSWORD,NOMBRE,APELLIDO,TELEFONO,DIRECCION,CORREO,APROBADA,TIPO_TRANSACCION FROM SecureCoding.CLIENTES");
        return ClienteClass::getAList($stmt);
    }

    public static function darClientePorID($login){
        $conexion = DataBaseClass::darConexion();
        $stmt = $conexion->prepare("SELECT LOGIN, PASSWORD,NOMBRE,APELLIDO,TELEFONO,DIRECCION,CORREO,APROBADA,TIPO_TRANSACCION FROM SecureCoding.CLIENTES WHERE LOGIN=?");
        $stmt->bind_param("s",$login);
        return ClienteClass::getAnElement($stmt);
    }

    public static function darClientesNoAprobados(){
        $conexion = DataBaseClass::darConexion();
        $stmt = $conexion->prepare("SELECT LOGIN, PASSWORD,NOMBRE,APELLIDO,TELEFONO,DIRECCION,CORREO,APROBADA,TIPO_TRANSACCION FROM SecureCoding.CLIENTES WHERE APROBADA=0");
        return ClienteClass::getAList($stmt);
    }

    public static function rechazarRegistro($login){
        $conexion = DataBaseClass::darConexion();
        $cliente = ClienteClass::darClientePorID($login);
        if(isset($cliente) && $cliente->aprobado==0){
            $stmt = $conexion->prepare("DELETE FROM SecureCoding.CLIENTES WHERE LOGIN=?");
            $stmt->bind_param("s",$login);
            if($stmt->execute()){
                return true;
            }
            return false;
        }
        return false;
    }

    //--------------------------------------------------------
    // Métodos
    //--------------------------------------------------------

    /**
     * Inicializa los datos de la carrera
     * @param $login String
     * @param $pass String
     * @param $nombre String
     * @param $apellido String
     * @param $telefono String
     * @param $direccion String
     * @param $correo String
     * @param $aprobada int
     * @param $tipo_transaccion
     */
    public function asignarDatosCliente($login, $pass, $nombre, $apellido, $telefono, $direccion, $correo, $aprobada, $tipo_transaccion)
    {
        $this->login=$login;
        $this->pass=$pass;
        $this->nombre=$nombre;
        $this->apellido=$apellido;
        $this->telefono=$telefono;
        $this->direccion=$direccion;
        $this->correo=$correo;
        $this->aprobado=$aprobada;
        $this->tipo_transaccion=$tipo_transaccion;
    }

    /**
     * Verifica si $password coincide con la contraseña del usuario
     * @param $password - la constraseña a analizar
     * @return bool - true si $password coincide con la contraseña del usuario false de lo contrario.
     */
    public function verificarPassword($password)
    {
        if(hash('sha512',$password) == $this->pass){
            return true;
        }
        return false;
    }

    public function aprobarRegistro(){
		$conexion = DataBaseClass::darConexion();
        if($this->aprobado==0){
            $stmt = $conexion->prepare("UPDATE SecureCoding.CLIENTES SET APROBADA = 1 WHERE LOGIN = ?");
            $stmt->bind_param("s",$this->login);
            if($stmt->execute()){
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * Guarda un cliente nuevo en la base de datos.
     * @return bool - True si fue posible guardar la carrera, false de lo contrario.
     */
    public function save()
    {
        $conexion = DataBaseClass::darConexion();
        $cliente =ClienteClass::darClientePorID($this->login);
        if(!isset($cliente)){
            $stmt = $conexion->prepare("INSERT INTO SecureCoding.CLIENTES (LOGIN,PASSWORD,NOMBRE,APELLIDO,TELEFONO,DIRECCION,CORREO,TIPO_TRANSACCION) VALUES (?,?,?,?,?,?,?,?)");
            $stmt->bind_param("sssssssi",$this->login,hash('sha512',$this->pass),$this->nombre,$this->apellido,$this->telefono,$this->direccion,$this->correo,$this->tipo_transaccion);
            if($stmt->execute()){
                return true;
            }
            return false;
        }
        else{
            $stmt = $conexion->prepare("UPDATE SecureCoding.CLIENTES SET NOMBRE=?, APELLIDO = ?, TELEFONO = ?, DIRECCION = ?, CORREO = ?, APROBADA = ?");
            $stmt->bind_param("sssssi",$this->nombre,$this->apellido,$this->telefono,$this->direccion,$this->correo,$this->aprobado);
            if($stmt->execute()){
                return true;
            }
            return false;
        }
    }
}