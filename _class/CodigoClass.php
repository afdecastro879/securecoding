<?php
/**
 * Created by PhpStorm.
 * User: Andres
 * Date: 17/06/14
 * Time: 10:00 AM
 */

include_once 'DataBaseClass.php';

class CodigoClass{
    //--------------------------------------------------------
    // Atributos
    //--------------------------------------------------------

    const NUMERO_CODIGOS_USUARIO = 100;
    const MONTO_MAXIMO = 10000;

    /**
     * Login de usuario
     */
    private $login;

    /**
     * Codigo
     */
    private $codigo;

    /**
     * Indica si el codigo fue usado
     */
    private $usado;

    /**
     * Inidca el identificador numerico para el codigo del cliente
     */
    private $numeral;

    //--------------------------------------------------------
    // Constructor
    //--------------------------------------------------------

    /**
     * Constructor de la clase
     */
    function __construct()
    {
    }

    //--------------------------------------------------------
    // Getter and Setters
    //--------------------------------------------------------

    /**
     * Getter de los atributos
     * @param $property - el nombre del atributo
     * @return mixed - el valor del atributo
     */
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    /**
     * Setter de los atributos
     * @param $property - el nombre del atributo
     * @param $value - el nuevo valor
     */
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

    //--------------------------------------------------------
    // Métodos Estaticos
    //--------------------------------------------------------

    /**
     * @param $preparedSentence mysqli_stmt
     * @return array
     */
    private static function getAList($preparedSentence){
        $codigos = array();
        $login_cliente = $code = $usado = $numeral = null;
        if($preparedSentence->execute()){
            $preparedSentence->bind_result($login_cliente,$code,$usado,$numeral);
            while($preparedSentence->fetch())
            {
                $codigo = new TransaccionClass();
                $codigo->asignarDatosCodigo($login_cliente,$code,$usado,$numeral);
                array_push($codigos,$codigo);
            }
        }
        $preparedSentence->close();
        return $codigos;
    }

    /**
     * @param $preparedSentence mysqli_stmt
     * @return CodigoClass|null
     */
    private static function getAnElement($preparedSentence){
        $login_cliente = $code = $usado = $numeral = null;
        $codigo = null;
        if($preparedSentence->execute()){
            $preparedSentence->bind_result($login_cliente,$code,$usado,$numeral);
            if($preparedSentence->fetch()){
                $codigo = new CodigoClass();
                $codigo->asignarDatosCodigo($login_cliente,$code,$usado,$numeral);
            }
        }
        return $codigo;
    }

    public static function darCodigoPorNumero($num, $login){
        $conexion = DataBaseClass::darConexion();
        $stmt = $conexion->prepare("SELECT LOGIN_CLIENTE,CODIGO,USADO,NUMERAL FROM SecureCoding.CODIGOS WHERE NUMERAL=? AND LOGIN_CLIENTE=?");
        $stmt->bind_param("is",$num,$login);
        return CodigoClass::getAnElement($stmt);
    }

    public static function generarCodigos($login){
        $codigos = array();
        for($i=0; $i<CodigoClass::NUMERO_CODIGOS_USUARIO; $i++){
            $token = md5(uniqid(mt_rand(), true));
            $token = substr($token,0,15);
            array_push($codigos,$token);
        }
        $i = 1;
        $conexion = DataBaseClass::darConexion();
        $stmt = $conexion->prepare("INSERT INTO SecureCoding.CODIGOS (LOGIN_CLIENTE, CODIGO, NUMERAL) VALUES (?,?,?)");
        $code = null;
        $stmt->bind_param("ssi",$login,$code,$i);
        foreach($codigos as $codigo){
            $code = hash('sha512',$codigo);
            $stmt->execute();
            $i++;
        }
        return $codigos;
    }

    public static function darNumeralCodigoValido($login){
        $conexion = DataBaseClass::darConexion();
        $stmt = $conexion->prepare("SELECT LOGIN_CLIENTE,CODIGO,USADO,NUMERAL FROM SecureCoding.CODIGOS WHERE USADO=0 AND
            LOGIN_CLIENTE=? ORDER BY NUMERAL ASC LIMIT 1");
        $stmt->bind_param("s",$login);
        $codigo = CodigoClass::getAnElement($stmt);
        return $codigo->numeral;
    }

    public static function verificarCodigo($codigo,$numeral,$login){
        $solicitado = CodigoClass::darCodigoPorNumero($numeral,$login);
        $usado = $solicitado->usado;
        if($usado == 0){
            if($solicitado->codigo == hash('sha512',$codigo)){
                $conexion = DataBaseClass::darConexion();
                $stmt = $conexion->prepare("UPDATE SecureCoding.CODIGOS SET USADO=1 WHERE NUMERAL=? AND LOGIN_CLIENTE=?");
                $stmt->bind_param("is",$numeral,$login);
                if($stmt->execute()){
                    return true;
                }
                return false;
            }
            return false;
        }
        return false;
    }

    public static function insertarCodigoToken($codigo,$numeral,$login){
        $conexion = DataBaseClass::darConexion();
        $stmt = $conexion->prepare("INSERT INTO SecureCoding.CODIGOS (LOGIN_CLIENTE, CODIGO, NUMERAL) VALUES (?,?,?)");
        $stmt->bind_param("ssi",$login,$codigo,$numeral);
        if($stmt->execute()){
            return true;
        }
        return false;
    }

	public static function verificarCodigoGeneradoConToken($cuentaLlegada,$monto,$pin,$codigo){
        $array = array();
        $i=3;
        $resultado = exec("../_bl/verificator $cuentaLlegada $monto $pin $codigo",$array,$i);
        if($i==0){
            return true;
        }
		// TODO: Hacer el alogritmo aqui de comparacion del codigo con los datos de la transaccion! retornar true o false
		return false;
    }
	
    //--------------------------------------------------------
    // Métodos
    //--------------------------------------------------------

    /**
     * Inicializa los datos de la carrera
     */
    public function asignarDatosCodigo($login, $codigo, $usado, $numeral)
    {
        $this->login=$login;
        $this->codigo=$codigo;
        $this->usado=$usado;
        $this->numeral=$numeral;
    }
}