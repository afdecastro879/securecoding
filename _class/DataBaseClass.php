<?php
/**
 * Class DataBaseClass Realiza la conexion con la base de datos.
 */
class DataBaseClass {

    private static $conexion;

    public static function darConexion(){

        if(!isset(DataBaseClass::$conexion)){
            $host = "localhost";
            $username = "SecureCoding";
            $password = "SecureCoding13579!!";
            $dbname = "SecureCoding";

            $conexion = new mysqli($host,$username,$password,$dbname);
            if($conexion->connect_error)
            {
                throw new Exception("Error de conexion con la base de datos");
            }
            $conexion->select_db($dbname);
            $conexion->query("SET NAMES 'utf8'");
            $conexion->query("SET SESSION time_zone = 'America/Bogota'");
            date_default_timezone_set("America/Bogota");
        }
        return $conexion;
    }
}